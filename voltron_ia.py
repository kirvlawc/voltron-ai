import streamlit as st
import numpy as np
import cv2
from PIL import Image
from tensorflow.keras.preprocessing import image
from tensorflow import keras

def classification():
    LABELS = ['HEALTHY', 'BLACK_ROT', 'ESCA', 'LEAF_BLIGHT']
    images = []
    img = cv2.imread('out.jpg')
    img = cv2.resize(img, (256, 256))
    img = img.astype('float32') / 255  
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    images.append(img_rgb)
    model = keras.models.load_model('voltron_disease.h5')
    classes = model.predict(np.asarray(images)) # predicting images
    result = np.argmax(classes)
    return LABELS[result]

def main():
    st.set_option('deprecation.showfileUploaderEncoding', False)
    st.header("Reconnaissance d'image")
    st.write("Veuillez charger une image de vigne")

    image_file = st.file_uploader("Upload Image",type=['jpg', 'jpeg'])
    if image_file is not None:
        our_image = Image.open(image_file)
        our_image.save('out.jpg')
        if st.button('Process'):
            st.success(classification())
        st.image(our_image , use_column_width=True,channels='RGB')

if __name__ == "__main__":
    main()
